import fs from 'fs'

module.exports = class PromisifyFs {
  static readdir(path) {
    return new Promise((resolve, reject) => {
      fs.readdir(path, (err, items) => {
        if (err) reject(err)
        resolve(items)
      })
    })
  }

  static getDirectory(path) {
    return new Promise((resolve, reject) => {
      fs.readdir(path, (err, items) => {
        if (err) reject(err)
        resolve(items.filter(f => fs.statSync(`${path}/${f}`).isDirectory()))
      })
    })
  }

  static async asyncForEach(array, callback) {
    // eslint-disable-next-line no-plusplus
    for (let index = 0; index < array.length; index++) {
      // eslint-disable-next-line no-await-in-loop
      await callback(array[index], index, array)
    }
  }
}

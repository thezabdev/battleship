import joi from '@hapi/joi'

const mongoConnectSchema = joi
  .object()
  .keys({
    MONGO_HOST: joi.string().required(),
    MONGO_PORT: joi.string().required(),
    MONGO_DATABASE_NAME: joi.string().required(),
    MONGO_USERNAME: joi.string().required(),
    MONGO_PASSWORD: joi.string().required()
  })
  .unknown()
  .required()

const { error, value: mongoConfig } = joi.validate(process.env, mongoConnectSchema)
if (error) {
  throw new Error(`Mongo configuration validation error: ${error.message}`)
}

const username = mongoConfig.MONGO_USERNAME
const password = mongoConfig.MONGO_PASSWORD
const host = mongoConfig.MONGO_HOST
const port = mongoConfig.MONGO_PORT

const connectionStringUri =
  username && password
    ? `mongodb://${username}:${encodeURIComponent(password)}@${host}:${port}/?authMechanism=DEFAULT`
    : `mongodb://${host}:${port}/?authMechanism=DEFAULT`

const config = {
  mongo: {
    username,
    password,
    host,
    port,
    databaseName: mongoConfig.MONGO_DATABASE_NAME,
    connectionStringUri
  }
}

export default config

import InitialGame from '../services/initialGame'
import PlaceShip from '../services/placeShip'
import Attack from '../services/attack'
import GetOceanInfo from '../services/getOceanInfo'

exports.getOceanInfo = async (req, res) => {
  try {
    const ocean = await new GetOceanInfo().perform()

    if (!ocean) {
      return res.status(200).send({
        message: 'No game found. Please reset to start new game.'
      })
    }
    return res.status(200).send({
      data: ocean
    })
  } catch (error) {
    return res.status(500).send(error)
  }
}

exports.reset = async (req, res) => {
  try {
    await new InitialGame().perform()
    return res.status(200).send({
      message: 'reset successfully'
    })
  } catch (error) {
    return res.status(500).send(error)
  }
}

exports.placeShip = async (req, res) => {
  try {
    const { shipType, coordinate, shipDirection } = req.body
    await new PlaceShip(shipType, coordinate, shipDirection).perform()
    return res.status(201).send({ message: `placed ${shipType}` })
  } catch (error) {
    return res.status(400).send({
      message: error.message
    })
  }
}

exports.attack = async (req, res) => {
  try {
    const { x, y } = req.body
    const attackResult = await new Attack(x, y).perform()
    return res.status(200).send({
      message: attackResult
    })
  } catch (error) {
    return res.status(400).send({
      message: error.message
    })
  }
}

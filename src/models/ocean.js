import _ from 'lodash'

import Coordinate from './coordinate'
import Cell from './cell'

const startOceanIndex = 0
const getPosition = (ship, coordinate) => {
  const { width, long } = ship.size
  const { x, y } = coordinate
  const position = []
  for (let indexX = x; indexX < x + width; indexX += 1) {
    for (let indexY = y; indexY < y + long; indexY += 1) {
      position.push(new Coordinate(indexX, indexY))
    }
  }
  return position
}

const oceanState = {
  new: 'new',
  ready: 'ready',
  playing: 'playing',
  gameOver: 'gameOver'
}

const validaShipDirection = ['horizontal', 'vertical']

export default class Ocean {
  constructor({ id, width = 10, long = 10, state = oceanState.new, fleet = [], attackHistory = [] }) {
    this.id = id
    this.state = state
    this.width = width
    this.long = long
    this.fleet = fleet
    this.attackHistory = attackHistory
  }

  get isGameOver() {
    return this.fleet.every(ship => ship.sank)
  }

  get shotInformation() {
    const totalShots = this.attackHistory.length
    const missedShots = this.attackHistory.filter(cell => cell.isMiss).length
    return { totalShots, missedShots }
  }

  set gameState(state) {
    this.state = state
  }

  startPlayingGame() {
    this.gameState = oceanState.playing
  }

  placeShip(shipType, coordinate, shipDirection) {
    try {
      const ship = this.getShip(shipType)
      if (!ship) {
        throw new Error(`No ship with type ${shipType} or All ${shipType} already placed`)
      }

      if (!shipDirection || !validaShipDirection.includes(shipDirection.toLowerCase())) {
        throw new Error(`Ship direction accept only ${validaShipDirection.join('and ')}`)
      }

      ship.rotate(shipDirection)

      const position = getPosition(ship, coordinate)

      if (!this.isShipPlaceInOceanArea(position)) {
        throw new Error('Ship position is exceed ocean area')
      }
      const coordinatesAroundThePosition = this.getCoordinatesAround(position)
      if (this.haveFleetInCoordinates([...coordinatesAroundThePosition, ...position])) {
        throw new Error('Already have ship near position')
      }

      const cells = position.map(cood => new Cell(cood))
      ship.placeShip(cells)

      if (this.allShipPlacedInOcean()) {
        this.state = oceanState.ready
      }
      return
    } catch (error) {
      throw error
    }
  }

  attack(coordinate) {
    if (!this.isInOceanArea(coordinate)) {
      throw new Error('Coordinate is exceed ocean area')
    }

    if (this.alreadyAttackedOnCoordinate(coordinate)) {
      throw new Error('Player attempt on attck any attacked coordinate')
    }
    this.startPlayingGame()
    const attckedShip = this.hitOnFleet(coordinate)
    if (!attckedShip) {
      const missedCell = new Cell(coordinate)
      missedCell.miss()
      this.addAttackHistory(missedCell)
      return missedCell.state
    }

    const attackedCell = attckedShip.getCellAt(coordinate)
    this.addAttackHistory(attackedCell)

    if (this.isGameOver) {
      this.gameState = oceanState.gameOver
      const { totalShots, missedShots } = this.shotInformation
      return `Game over with total: ${totalShots} shots and ${missedShots} missed shots`
    }

    if (attckedShip.sank) {
      return `You just sank ${attckedShip.type}`
    }

    return attackedCell.state
  }

  get readyToPlay() {
    const validStateToPlay = [oceanState.ready, oceanState.playing]
    return validStateToPlay.includes(this.state)
  }

  getShip(shipType) {
    if (!shipType) return null
    return this.fleet
      .filter(ship => !ship.placedInOcean)
      .find(ship => ship.type.toLowerCase() === shipType.toLowerCase())
  }

  isShipPlaceInOceanArea(position) {
    return position.every(coordinate => {
      const { x, y } = coordinate
      return startOceanIndex <= x && x < this.width && (startOceanIndex <= y && y < this.long)
    })
  }

  isInOceanArea({ x, y }) {
    return startOceanIndex <= x && x < this.width && (startOceanIndex <= y && y < this.long)
  }

  getCoordinatesAround(position) {
    const offsetCoordinates = [
      new Coordinate(-1, -1),
      new Coordinate(0, -1),
      new Coordinate(1, -1),
      new Coordinate(-1, 0),
      new Coordinate(1, 0),
      new Coordinate(-1, 1),
      new Coordinate(0, 1),
      new Coordinate(1, 1)
    ]

    const coords = position.reduce((coordinates, coordinate) => {
      const newCoordinate = offsetCoordinates
        .map(offsetCoordinate => coordinate.plus(offsetCoordinate))
        .filter(cood => this.isInOceanArea(cood))
      return [...coordinates, ...newCoordinate]
    }, [])

    const isInPosition = coordinate => {
      return (
        position.filter(anotherCoordinate => {
          return anotherCoordinate.equal(coordinate)
        }).length > 0
      )
    }

    return _.uniqWith(coords.filter(coordinate => !isInPosition(coordinate)), _.isEqual)
  }

  haveFleetInCoordinates(coordinates) {
    return this.fleet
      .filter(ship => ship.placedInOcean)
      .some(ship => {
        return coordinates.some(coordinate => {
          return ship.isPlaceInCoordinate(coordinate)
        })
      })
  }

  allShipPlacedInOcean() {
    return this.fleet.every(ship => ship.placedInOcean)
  }

  alreadyAttackedOnCoordinate(coordinate) {
    if (!this.attackHistory || this.attackHistory.length === 0) return false

    return this.attackHistory.some(cell => cell.isSameCoordinate(coordinate))
  }

  hitOnFleet(coordinate) {
    // console.log(this.fleet)
    return this.fleet.find(ship => ship.getAttacked(coordinate))
  }

  addAttackHistory(cell) {
    if (!this.attackHistory || this.attackHistory.length === 0) {
      this.attackHistory = []
    }
    this.attackHistory.push(cell)
  }
}

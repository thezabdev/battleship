const shipDirection = {
  vertical: 'vertical',
  horizontal: 'horizontal'
}

const shipState = {
  clean: 'clean',
  hit: 'hit',
  sank: 'sank'
}
export default class Ship {
  constructor({
    type,
    size = { width: 1, long: 1 },
    state = shipState.clean,
    direction = 'horizontal',
    position = null
  }) {
    this.type = type
    this.size = size
    this.state = state
    this.direction = direction
    this.position = position
  }

  get placedInOcean() {
    return this.position !== null && this.position !== undefined
  }

  get sank() {
    return this.position.every(cell => cell.isHit)
  }

  rotate(direction) {
    if (this.direction === direction) return

    switch (direction) {
      case shipDirection.vertical:
        this.size = { width: this.size.long, long: this.size.width }
        this.direction = shipDirection.vertical
        break
      case shipDirection.horizontal:
        this.size = { width: this.size.long, long: this.size.width }
        this.direction = shipDirection.horizontal
        break
      default:
        break
    }
  }

  isPlaceInCoordinate(coordinate) {
    if (!this.placedInOcean) return false

    return this.position
      .map(cell => cell.coordinate)
      .some(cood => {
        return cood.equal(coordinate)
      })
  }

  placeShip(cells) {
    this.position = cells
  }

  getAttacked(coordinate) {
    if (!this.isPlaceInCoordinate(coordinate)) return null
    const attackedCell = this.getCellAt(coordinate)

    if (attackedCell) {
      attackedCell.hit()
      this.state = this.sank ? shipState.sank : shipState.hit
      return attackedCell
    }
    return null
  }

  getCellAt(coordinate) {
    return this.position.find(cell => cell.isSameCoordinate(coordinate))
  }
}

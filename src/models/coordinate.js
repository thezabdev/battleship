export default class Coordinate {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  plus(anotherCoordinate) {
    const { x, y } = anotherCoordinate
    return new Coordinate(this.x + x, this.y + y)
  }

  equal(anotherCoordinate) {
    const { x, y } = anotherCoordinate
    return this.x === x && this.y === y
  }
}

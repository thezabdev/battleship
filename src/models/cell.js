const cellState = {
  clean: 'clean',
  hit: 'hit',
  miss: 'miss'
}

export default class Cell {
  constructor(coordinate = { x: 0, y: 0 }, state = cellState.clean) {
    this.coordinate = coordinate
    this.state = state
  }

  isSameCoordinate(anotherCoordinate) {
    return this.coordinate.equal(anotherCoordinate)
  }

  hit() {
    this.state = cellState.hit
  }

  miss() {
    this.state = cellState.miss
  }

  get isHit() {
    return this.state === cellState.hit
  }

  get isMiss() {
    return this.state === cellState.miss
  }
}

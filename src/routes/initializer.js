/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import consola from 'consola'
import path from 'path'
import fs from 'fs'
import notFoundRouter from './notFound'
import PromisifyUtil from '../utilities/promisifyUtil'

module.exports = class Initializer {
  constructor(app) {
    this.app = app
  }

  async init() {
    consola.start('Initializing router...')
    const root = path.dirname(require.main.filename)
    const versionDirs = await PromisifyUtil.getDirectory(path.join(root, 'routes'))

    await PromisifyUtil.asyncForEach(versionDirs, async versionDir => {
      const publicDirectory = path.join(root, `routes/${versionDir}`)
      if (fs.existsSync(publicDirectory)) {
        const files = await PromisifyUtil.readdir(publicDirectory)
        files.forEach(file => {
          consola.info(`Adding public router: ${versionDir} - ${file}`)
          const router = require(path.join(publicDirectory, file))
          this.app.use(`/${versionDir}`, router)
        })
      }
    })

    this.app.use('*', notFoundRouter)
  }
}

import express from 'express'
import routeController from '../controllers/routeController'

const router = express.Router()

router.get('/', routeController.notFound)

module.exports = router

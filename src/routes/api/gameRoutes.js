import express from 'express'
import gameController from '../../controllers/gameController'

const router = express.Router()

router.get('/', gameController.getOceanInfo)
router.post('/ship', gameController.placeShip)
router.post('/reset', gameController.reset)
router.post('/attack', gameController.attack)

module.exports = router

import gameRepository from '../repositories/gameRepository'

export default class PlaceShip {
  constructor(shipType, coordinate, shipDirection) {
    this.shipType = shipType
    this.coordinate = coordinate
    this.shipDirection = shipDirection
    this.gameRepository = gameRepository
  }

  async perform() {
    try {
      const ocean = await this.gameRepository.getLatestOcean()
      if (!ocean) {
        return Promise.reject(new Error('No Ocean found'))
      }
      ocean.placeShip(this.shipType, this.coordinate, this.shipDirection)
      await this.gameRepository.updateOcean(ocean)
      return Promise.resolve()
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

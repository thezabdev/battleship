import gameRepository from '../repositories/gameRepository'
import Coordinate from '../models/coordinate'

const isValidArgument = val => val !== null && val !== undefined
export default class Attack {
  constructor(x = -1, y = -1) {
    if (!isValidArgument(x) || !isValidArgument(y)) {
      throw new Error('invalid argument for attack at coordinate')
    }
    this.coordinate = new Coordinate(x, y)
    this.gameRepository = gameRepository
  }

  async perform() {
    try {
      const ocean = await this.gameRepository.getLatestOcean()
      if (!ocean) {
        return Promise.reject(new Error('No Ocean found'))
      }

      if (ocean.isGameOver) {
        const { totalShots, missedShots } = ocean.shotInformation
        return Promise.resolve(
          `Game already over with total: ${totalShots} shots and ${missedShots} missed shots. Please reset to start new game.`
        )
      }

      if (!ocean.readyToPlay) {
        return Promise.reject(new Error('Unauthorized'))
      }

      const attackResult = ocean.attack(this.coordinate)
      await this.gameRepository.updateOcean(ocean)
      return Promise.resolve(attackResult)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

import gameRepository from '../repositories/gameRepository'

export default class GetOceanInfo {
  constructor() {
    this.gameRepository = gameRepository
  }

  async perform() {
    try {
      const ocean = await this.gameRepository.getLatestOcean()
      return Promise.resolve(ocean)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

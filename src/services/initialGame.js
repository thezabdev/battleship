import gameRepository from '../repositories/gameRepository'
import Ocean from '../models/ocean'
import Ship from '../models/ship'

export default class InitialGame {
  constructor() {
    this.gameRepository = gameRepository
  }

  async perform() {
    try {
      const newOcean = new Ocean({
        fleet: [
          new Ship({ type: 'Battleship', size: { width: 4, long: 1 } }),
          new Ship({ type: 'Cruiser', size: { width: 3, long: 1 } }),
          new Ship({ type: 'Cruiser', size: { width: 3, long: 1 } }),
          new Ship({ type: 'Destroyer', size: { width: 2, long: 1 } }),
          new Ship({ type: 'Destroyer', size: { width: 2, long: 1 } }),
          new Ship({ type: 'Destroyer', size: { width: 2, long: 1 } }),
          new Ship({ type: 'Submarine', size: { width: 1, long: 1 } }),
          new Ship({ type: 'Submarine', size: { width: 1, long: 1 } }),
          new Ship({ type: 'Submarine', size: { width: 1, long: 1 } }),
          new Ship({ type: 'Submarine', size: { width: 1, long: 1 } })
        ]
      })
      const ocean = await this.gameRepository.getLatestOcean()
      if (!ocean) {
        await this.gameRepository.createNewOcean(newOcean)
      } else {
        newOcean.id = ocean.id
        await this.gameRepository.updateOcean(newOcean)
      }
      return Promise.resolve()
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

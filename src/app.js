import consola from 'consola'
import express from 'express'
import expressValidator from 'express-validator'
import logger from 'morgan'
import http from 'http'
import cors from 'cors'
import RouteInitializer from './routes/initializer'

const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 3000
const app = express()
const server = http.Server(app)

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())
app.use(expressValidator())

const routerInitializer = new RouteInitializer(app)

async function start() {
  await routerInitializer.init()

  server.listen(port, host, () => {
    consola.ready(`Server listening on http://${host}:${port}`)
  })
}
start()

module.exports = { app }

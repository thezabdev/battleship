/* eslint-disable no-underscore-dangle */
import { ObjectId } from 'mongodb'
import MongoConnector from './mongoConnector'
import Ocean from '../models/ocean'
import Ship from '../models/ship'
import Coordinate from '../models/coordinate'
import Cell from '../models/cell'

class GameRepository extends MongoConnector {
  constructor(collectionName) {
    super()

    this.collectionName = collectionName
    this.validate()
  }

  async createNewOcean(ocean) {
    const { width, long, state, fleet, attackHistory } = ocean
    return this.create({ width, long, state, fleet, attackHistory })
  }

  async getLatestOcean() {
    return new Promise(async (resolve, reject) => {
      try {
        const latestOceans = await this.find({})
        if (latestOceans.length > 0) {
          const latestOcean = latestOceans[latestOceans.length - 1]
          const attackHistory = latestOcean.attackHistory
            ? latestOcean.attackHistory.map(cell => {
                const { x, y } = cell.coordinate
                return new Cell(new Coordinate(x, y), cell.state)
              })
            : []
          const ocean = new Ocean({
            ...latestOcean,
            id: latestOcean._id.toString(),
            fleet: latestOcean.fleet.map(ship => {
              const position = ship.position
                ? ship.position.map(cell => {
                    const { x, y } = cell.coordinate
                    return new Cell(new Coordinate(x, y), cell.state)
                  })
                : null
              return new Ship({ ...ship, position })
            }),
            attackHistory
          })
          resolve(ocean)
        }
        resolve(null)
      } catch (error) {
        reject(error)
      }
    })
  }

  async updateOcean(ocean) {
    return new Promise(async (resolve, reject) => {
      try {
        const { id, width, long, state, fleet, attackHistory } = ocean
        this.updateOne({ _id: ObjectId(id) }, { $set: { width, long, state, fleet, attackHistory } }).then(result => {
          resolve(result)
        })
      } catch (error) {
        reject(error)
      }
    })
  }
}

module.exports = new GameRepository('games')

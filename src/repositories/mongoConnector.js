import MongoDB from 'mongodb'

import config from '../config'

const { MongoClient } = MongoDB

module.exports = class MongoConnector {
  constructor() {
    this.url = config.mongo.connectionStringUri
    this.dbName = config.mongo.databaseName
  }

  validate() {
    if (!this.collectionName) throw new TypeError('collectionName is not defined')
  }

  getCollection(callback, customCollectionName = this.collectionName) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        const result = await callback(db.collection(customCollectionName))
        connection.close()
        resolve(result)
      } catch (error) {
        reject(error)
      }
    })
  }

  create(data) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        let result
        if (!Array.isArray(data)) result = await db.collection(this.collectionName).insertOne(data)
        else
          result = await db
            .collection(this.collectionName)
            .insertMany(data)
            .toArray()
        connection.close()
        resolve(result.ops)
      } catch (error) {
        reject(error)
      }
    })
  }

  find(query, findAll = true) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        let result
        if (!findAll) result = await db.collection(this.collectionName).findOne(query)
        else {
          result = await db
            .collection(this.collectionName)
            .find(query)
            .toArray()

          if (result === null) result = []
        }
        connection.close()
        resolve(result)
      } catch (error) {
        reject(error)
      }
    })
  }

  update(query, data, updateMany = true) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        let result
        if (!updateMany) result = await db.collection(this.collectionName).updateOne(query, data)
        else result = await db.collection(this.collectionName).updateMany(query, data)
        connection.close()
        resolve(result)
      } catch (error) {
        reject(error)
      }
    })
  }

  updateOne(query, data) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        const result = await db.collection(this.collectionName).updateOne(query, data)
        connection.close()
        resolve(result)
      } catch (error) {
        reject(error)
      }
    })
  }

  delete(query, deleteMany = true) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        let result
        if (!deleteMany) result = await db.collection(this.collectionName).deleteOne(query)
        else result = await db.collection(this.collectionName).deleteMany(query)
        connection.close()
        resolve(result)
      } catch (error) {
        reject(error)
      }
    })
  }

  createIndex(indexParam) {
    return new Promise(async (resolve, reject) => {
      try {
        const client = new MongoClient(this.url, { useNewUrlParser: true })
        const connection = await client.connect()
        const db = await connection.db(this.dbName)
        const result = await db.collection(this.collectionName).createIndex(indexParam, null, null)
        connection.close()
        resolve(result)
      } catch (error) {
        reject(error)
      }
    })
  }
}

DEVELOPMENT_NAME = battleship-node-server-dev
IMAGE = battleship-node-server
TAG_PREFIX = production

default:
	echo 'CI/CD Pipeline V1'
 
dev:
	docker-compose -f dev.docker-compose.yml build
	docker-compose -f dev.docker-compose.yml up
 
down:
	docker-compose -f dev.docker-compose.yml down
 
test-build:
	docker build -f ./Dockerfile -t $(DEVELOPMENT_NAME) . --no-cache

test-up:
	docker-compose -f test.docker-compose.yml up

test-down:
	docker-compose -f test.docker-compose.yml down

build:
	docker build -f ./Dockerfile -t $(IMAGE):$(TAG_PREFIX) . --no-cache
 
push:
	docker push $(IMAGE):$(TAG_PREFIX)$(BUILD_TARGET)-$(BUILD_VERSION)
 
push-and-clean: push
	docker rmi $(IMAGE):$(TAG_PREFIX)$(BUILD_TARGET)-$(BUILD_VERSION)
 
in:
	docker exec -it $(DEVELOPMENT_NAME) sh
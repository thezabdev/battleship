# develop stage
FROM node:10-alpine as develop-stage
WORKDIR /usr/src/app
COPY ./package*.json ./
RUN npm install
COPY . .
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.1/wait /wait
RUN chmod +x /wait
# build stage
FROM develop-stage as build-stage
RUN npm run build

#production stage
FROM build-stage
CMD sh -c "/wait && node dist/app.js"

# FROM node:10-alpine
# WORKDIR /system
# COPY . /system-temp
# RUN cd /system-temp && npm install && npm run build && cp -R dist /system && cp -R node_modules /system
# RUN rm -rf /system-temp
# CMD node dist/app.js
# Battleship

Battleship is a simulation board game with size 10 \* 10. When game start the ocean and the fleet consist of

- 1 x Battleship
- 2 x Cruiser
- 3 x Destroyer
- 4 x Submarine

Player must place all ship in the fleet before start attack. The game will end after attacker sank all ships in the fleet.
Player can operate with the game through API implemented with Node.JS, MongoDb and Docker.

## Installation

Battleship requires Docker and Docker compose to run.

- Docker Engine (>= 19.03.1)
  Follow [Docker Install Instructions](https://docs.docker.com/install/). After install you can check by

    ```
    docker -v
    ```

- Docker Compose (>= 1.24.1)
  You can skip this step if you've already installed. If you not install please follow [Docker Compose Install Instructions](https://docs.docker.com/compose/install/). After install you can check by

    ```
    docker-compose --version
    ```

## Get started to run software

1. clone source code from Bitbucket [https://bitbucket.org/thezabdev/battleship/src/master/](https://bitbucket.org/thezabdev/battleship/src/master/)

    ```
    git clone https://thezabdev@bitbucket.org/thezabdev/battleship.git
    ```

2. Access to source code directory by

    ```
    cd ./battleship
    ```

3. build docker image by

    ```
    docker build -t battleship-node-server:production . --no-cache
    ```

4. Run container from image that built from previous step

    ```
    docker-compose up
    ```

5. Let fun.

## Battleship API

You can start to access API with base URL: **<http://localhost>**

## Request

### Get the current state of the ocean and the fleet

```sh
GET /api
```

### Reset the game to an initial state

```sh
POST /api/reset
```

### Place a ship into the ocean

```sh
 POST /api/ship
 content-type: application/json

 {
   "shipType": "Battleship", // Possible value is one of these type "Battleship", "Cruiser", "Destroyer", "Submarine"
   "coordinate": {
     "x": 2,
     "y": 2
   },
   "shipDirection": "vertical" // Possible value is one of these value "horizontal" and "vertical"
 }
```

### Attack to specific target on the ocean

```sh
 POST /api/attack
 content-type: application/json

 {
     "x": 3,
     "y": 6
 }
```

## How to play

1. call api **/api/reset** to create new game.
2. Defender place a ship into the ocean by call **/api/ship** with data mention in Battleship API above.
3. After defender placed all ship into the ocean attacker call **/api/attack** with coordinate x and y to attack. Attacker will receive message from API based on the current game situation.

    - **miss** When hit nothing.
    - **hit** when a ship has been hit
    - **You just sane the <ship-type>** when the ship has been sank. Replace **<ship-type>** with ship type name
    - **Game over with total: <totalShots> shots and <missedShots> missed shots** when all ships has been sunk.

4. During play the game player can call **/api** to get current statee of the ocean and the fleet.

## How to access database

### Docker

1. Write this command to check conatiner id or container name

    ```
    docker ps
    ```

2. Write execute docker conatiner command to access MongoDB container
    ```
    docker exec -it <container-name or container id> sh
    ```
   Replace **<container-name or container id>** with container id or container name
3. access to MongoDB with

    ```
    mongo -u <username> -p <password>
    ```

    Replace **<username>** and **<password>** with username nad password inside docker-compose.yml file. Under section **environment** **MONGO_INITDB_ROOT_USERNAME** for username and **MONGO_PASSWORD** for password.

4. Use [Mongo Shell Command](https://docs.mongodb.com/manual/reference/mongo-shell/) to access database and data

### GUI Tool for MongoDB

There are a lot of Graphic User Interface to access MongoDB. The list below is just example

- [Compass](https://www.mongodb.com/download-center/compass)
- [Studio 3T or Robo 3T](https://robomongo.org/download)
